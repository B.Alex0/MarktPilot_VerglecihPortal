import requests
from bs4 import BeautifulSoup

#########Erste etape mit Filter#########
URL_DMC = 'https://www.wollplatz.de/wolle?br=23&limit=filterbr'
URL_Drops = 'https://www.wollplatz.de/wolle?br=38&limit=filterbr' # Safran  und Baby Merino Mix
URL_Stylecraft = 'https://www.wollplatz.de/wolle?br=34&limit=filterbr'
URL_Liferung = 'https://www.wollplatz.de/versandkosten-und-lieferung'

response_DMC = requests.get(URL_DMC)
response_Drops = requests.get(URL_Drops)
response_Stylecraft = requests.get(URL_Stylecraft)
response_Liferung = requests.get(URL_Liferung)

soup_DMC = BeautifulSoup(response_DMC.text, 'html.parser') 
soup_Drops = BeautifulSoup(response_Drops.text, 'html.parser')
soup_Stylecraft = BeautifulSoup(response_Stylecraft.text, 'html.parser')
soup_Liferung = BeautifulSoup(response_Liferung.text, 'html.parser')

#Liferung fuer alle
perm_l = '' #Lefirzeit
soup_Liferung = soup_Liferung.find_all('td', {'style' : 'border-top: 1px inset; border-right: 1px inset; border-bottom: 1px inset; border-left: 1px inset'}) 
for i in range(len(soup_Liferung)):
    if soup_Liferung[i].text == 'Deutschland':
        perm_l = soup_Liferung[i + 4].text

#########Zweite Etape#########

######Fuer DMC

    ####Preis

for link in soup_DMC.find_all('a', {'title' : 'DMC Natura XL'}):
    link_page = link.get('href')
response_DMC_link = requests.get(link_page)
soup_DMC_link = BeautifulSoup(response_DMC_link.text, 'html.parser')

perm = []
price_data_DMC = soup_DMC_link.find_all('span', {'class' : 'product-price-amount'})
for i in range(len(price_data_DMC)):
    if price_data_DMC[i].text == '0,00':
        continue
    perm.append(price_data_DMC[i].text)
#print('Preise für DMC sind von', min(perm), '€','bis', max(perm), '€')

    ####Zusammenstellung + Nadelstärke

perm_z = [] #Zusammenstellung
perm_n = [] #Nadelstärke
zusamm_data_DMC = soup_DMC_link.find('div', {'id' : 'pdetailTableSpecs'})
test = zusamm_data_DMC.find_all('td')
for i in range(len(test)):
    if test[i].text == 'Zusammenstellung':
        perm_z.append(test[i + 1].text)
    elif test[i].text == 'Nadelstärke':
        perm_n.append(test[i + 1].text)
#print('Zusammenstellung:', *perm_z)
#print('Nadelstärke:', *perm_n)

    ####Lieferzeiten

lifern_data_DMC = soup_DMC_link.find('span', {'class' : 'stock-green'})
liferzeit = ''
if 'Begrenzter Vorrat' in lifern_data_DMC.text or 'Lieferbar' in lifern_data_DMC.text:
    liferzeit = perm_l
else:
    liferzeit = 'Liferung ist nicht moeglich'
#print(liferzeit)

    ###eintragen Daten in txt

with open('vergleich_tabelle.txt', 'a') as f:
    f.write(f"DMC\n")
    f.write(f"\n")
    f.write(f"Preis von {min(perm)} bis {max(perm)}\n")
    f.write(f"Zusammenstellung: {perm_z} Nadelstärke: {perm_n}\n")
    f.write(f"Liferzeit: {liferzeit}\n")
    f.write(f"\n")


######Fuer Stylecraft

for link in soup_Stylecraft.find_all('a', {'title' : 'Stylecraft Special DK'}):
    link_page = link.get('href')
response_Stylecraft_link = requests.get(link_page)
soup_Stylecraft_link = BeautifulSoup(response_Stylecraft_link.text, 'html.parser')

    ####Preis

perm = []
price_data_Stylecraft = soup_Stylecraft_link.find_all('span', {'class' : 'product-price-amount'})
for i in range(len(price_data_Stylecraft)):
    if price_data_Stylecraft[i].text == '0,00':
        continue
    perm.append(price_data_Stylecraft[i].text)
#print('Preise für Stylecraft sind von', min(perm), '€','bis', max(perm), '€')
    
    ####Zusammenstellung + Nadelstärke

perm_z = [] #Zusammenstellung
perm_n = [] #Nadelstärke
zusamm_data_Stylecraft = soup_Stylecraft_link.find('div', {'id' : 'pdetailTableSpecs'})
test = zusamm_data_Stylecraft.find_all('td')
for i in range(len(test)):
    if test[i].text == 'Zusammenstellung':
        perm_z.append(test[i + 1].text)
    elif test[i].text == 'Nadelstärke':
        perm_n.append(test[i + 1].text)
#print('Zusammenstellung:', *perm_z)
#print('Nadelstärke:', *perm_n)

    ####Lieferzeiten

lifern_data_Stylecraft = soup_Stylecraft_link.find('span', {'class' : 'stock-red'})
liferzeit = ''
if 'Begrenzter Vorrat' in lifern_data_Stylecraft.text or 'Lieferbar' in lifern_data_Stylecraft.text:
    liferzeit = perm_l
else:
    liferzeit = 'Liferung ist nicht moeglich'
#print(liferzeit)

    ###eintragen Daten in txt

with open('vergleich_tabelle.txt', 'a') as f:
    f.write(f"Stylecraft\n")
    f.write(f"\n")
    f.write(f"Preis von {min(perm)} bis {max(perm)}\n")
    f.write(f"Zusammenstellung: {perm_z} Nadelstärke: {perm_n}\n")
    f.write(f"Liferzeit: {liferzeit}\n")
    f.write(f"\n")

######Fuer Drops Baby Merino Mix

for link in soup_Drops.find_all('a', {'title' : 'Drops Baby Merino Mix'}):
    link_page = link.get('href')
response_Drops_link = requests.get(link_page)
soup_Drops_link = BeautifulSoup(response_Drops_link.text, 'html.parser')

    ####Preis

perm = []
price_data_Drops = soup_Drops_link.find_all('span', {'class' : 'product-price-amount'})
for i in range(len(price_data_Drops)):
    if price_data_Drops[i].text == '0,00':
        continue
    perm.append(price_data_Drops[i].text)
#print('Preise für Drops sind von', min(perm), '€','bis', max(perm), '€')

    ####Zusammenstellung + Nadelstärke

perm_z = [] #Zusammenstellung
perm_n = [] #Nadelstärke
zusamm_data_Drops = soup_Drops_link.find('div', {'id' : 'pdetailTableSpecs'})
test = zusamm_data_Drops.find_all('td')
for i in range(len(test)):
    if test[i].text == 'Zusammenstellung':
        perm_z.append(test[i + 1].text)
    elif test[i].text == 'Nadelstärke':
        perm_n.append(test[i + 1].text)
#print('Zusammenstellung:', *perm_z)
#print('Nadelstärke:', *perm_n)

    ####Lieferzeiten

lifern_data_Drops = soup_Drops_link.find('span', {'class' : 'stock-green'})
liferzeit = ''
if 'Begrenzter Vorrat' in lifern_data_Drops.text or 'Lieferbar' in lifern_data_Drops.text:
    liferzeit = perm_l
else:
    liferzeit = 'Liferung ist nicht moeglich'
#print(liferzeit)

    ###eintragen Daten in txt

with open('vergleich_tabelle.txt', 'a') as f:
    f.write(f"Drops Baby Merino Mix\n")
    f.write(f"\n")
    f.write(f"Preis von {min(perm)} bis {max(perm)}\n")
    f.write(f"Zusammenstellung: {perm_z} Nadelstärke: {perm_n}\n")
    f.write(f"Liferzeit: {liferzeit}\n")
    f.write(f"\n")

######Drops_Safran

for link in soup_Drops.find_all('a', {'title' : 'Drops Safran'}):
    link_page = link.get('href')
response_Drops_Safran_link = requests.get(link_page)
soup_Drops_Safran_link = BeautifulSoup(response_Drops_Safran_link.text, 'html.parser')

    ####Preis
    
perm = []
price_data_Drops_Safran = soup_Drops_Safran_link.find_all('span', {'class' : 'product-price-amount'})
for i in range(len(price_data_Drops_Safran)):
    if price_data_Drops_Safran[i].text == '0,00':
        continue
    perm.append(price_data_Drops_Safran[i].text)
#print('Preise für Drops_Safran sind von', min(perm), '€','bis', max(perm), '€')

    ####Zusammenstellung + Nadelstärke

perm_z = [] #Zusammenstellung
perm_n = [] #Nadelstärke
zusamm_data_Drops_Safran = soup_Drops_Safran_link.find('div', {'id' : 'pdetailTableSpecs'})
test = zusamm_data_Drops_Safran.find_all('td')
for i in range(len(test)):
    if test[i].text == 'Zusammenstellung':
        perm_z.append(test[i + 1].text)
    elif test[i].text == 'Nadelstärke':
        perm_n.append(test[i + 1].text)
#print('Zusammenstellung:', *perm_z)
#print('Nadelstärke:', *perm_n)

    ####Lieferzeiten

lifern_data_Drops_Safran = soup_Drops_Safran_link.find('span', {'class' : 'stock-green'})
liferzeit = ''
if 'Begrenzter Vorrat' in lifern_data_Drops_Safran.text or 'Lieferbar' in lifern_data_Drops_Safran.text:
    liferzeit = perm_l
else:
    liferzeit = 'Liferung ist nicht moeglich'
#print(liferzeit)

    ###eintragen Daten in txt

with open('vergleich_tabelle.txt', 'a') as f:
    f.write(f"Drops Safran\n")
    f.write(f"\n")
    f.write(f"Preis von {min(perm)} bis {max(perm)}\n")
    f.write(f"Zusammenstellung: {perm_z} Nadelstärke: {perm_n}\n")
    f.write(f"Liferzeit: {liferzeit}\n")
    f.write(f"\n")



